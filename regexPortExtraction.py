import tkinter as tk   # import the tkinter module as tk for GUI support
from tkinter import filedialog   # import filedialog submodule from tkinter for opening a file dialog
import re   # import the re module for regular expression support

gui = tk.Tk()   # create a Tk object for the GUI
gui.withdraw()  # hide the GUI window

log_file = filedialog.askopenfilename()   # open a file dialog to select a log file, and store its path into "log_file"

with open(log_file, "r") as file:   # open the selected log file in read only mode
    contents = file.read()   # reads the contents of the file and stores that into "contents" for future use

# I used two different port patterns as I was unable to return all of the ports due to the difference in formatting.
# I added port_pattern2 so this could be used on other logs that may possibly have strings that did not occur inside of the example log.
# The port_patterns below look for the words "port" or "service" before turning a port number. port_pattern3 looks for "port" or "service" followed by a colon, then returns a 1-5 digit port number.

port_pattern1 = r"port\s+(\d{1,5})"   # a regular expression pattern to match "port", followed by one or more whitespaces, then by 1-5 digits, and then stores that into "port_pattern1"
port_pattern2 = r"service\s+(\d{1,5})" # a regular expression pattern to match "service", followed by one or more whitespaces, then by 1-5 digits, and then stores that into "port_pattern2"
port_pattern3 = r"(?:port|service):\D*(\d{1,5})"   # a regular expression pattern to match either "port" or "service" followed by any number of non-digit characters, 1-5 digits, and then stores that into "port_pattern3"

port_numbers = []   # create an empty list to store the extracted port numbers from the port_pattern variables

for match in re.finditer(port_pattern1, contents, re.IGNORECASE):   # search for matches of port_pattern1 in the log file contents
    port = match.group(1)   # extract the port number
    port_numbers.append(port)   # add the port number to the list of port numbers

for match in re.finditer(port_pattern2, contents, re.IGNORECASE):   # search for matches of port_pattern2 in the log file contents
    port = match.group(1)   # extract the port number
    port_numbers.append(port)   # add the port number to the list of port numbers

for match in re.finditer(port_pattern3, contents, re.IGNORECASE):   # search for matches of port_pattern3 in the log file contents
    port = match.group(1)   # extract the port number
    port_numbers.append(port)   # add the port number to the list of port numbers

print(port_numbers)   # print the list of extracted port numbers
